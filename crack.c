#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *src = fopen(dictionary, "r");
    char line[PASS_LEN];
    
    if(!src)
    {
        printf("Couldn't open dictionary\n");
    }
    // Loop through the dictionary file, one line
    // at a time.
    
    while(fgets(line, PASS_LEN, src) != NULL)
    {
        char *nl = strchr(line, '\n');
        if(nl != NULL)
        {
            *nl = '\0';
        }
        char *hash = md5(line, strlen(line));
        if(strcmp(hash, target) == 0)
        {
            printf("%s\t%s\n", hash, line);
        }
    }
    // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
    free(hash);
    // Free up memory?

    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    // Open the hash file for reading.
    FILE *hf;
    char line[HASH_LEN];
    
    hf = fopen(argv[1], "r");
    if(!hf)
    {
        printf("Can't open hash file\n");
    }
    
    // For each hash, crack it by passing it to crackHash
    while(fgets(line, HASH_LEN + 1, hf) != NULL)
    {
        char *nl = strchr(line, '\n');
        if(nl != NULL)
        {
            *nl = '\0';
        }
        crackHash(line, argv[2]);
    }
    
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    // Close the hash file
    fclose(hf);
    // Free up any malloc'd memory?
}
